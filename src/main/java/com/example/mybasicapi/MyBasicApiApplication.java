package com.example.mybasicapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyBasicApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyBasicApiApplication.class, args);
    }

}

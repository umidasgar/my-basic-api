package com.example.mybasicapi.service;

import com.example.mybasicapi.entity.Student;

import java.util.List;

public interface StudentService {

    List<Student> getAll();

    Student getById(long id);

    Student add(Student student);

    Student update(Student student);

    void delete(long id);
}

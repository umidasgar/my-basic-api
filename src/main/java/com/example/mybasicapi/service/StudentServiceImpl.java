package com.example.mybasicapi.service;

import com.example.mybasicapi.entity.Student;
import com.example.mybasicapi.repository.StudentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository repository;

    public StudentServiceImpl(StudentRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Student> getAll() {
        return repository.findAll();
    }

    @Override
    public Student getById(long id) {
        return repository.findById(id).orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Student with id " + id + " not found.");
        });
    }

    @Override
    public Student add(Student student) {
        return repository.save(student);
    }

    @Override
    public Student update(Student student) {

        if (!repository.existsById(student.getId())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Student with id " + student.getId() + " not found.");
        }
        return repository.save(student);
    }

    @Override
    public void delete(long id) {

        if (repository.existsById(id)) {
            repository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Student with id " + id + " not found.");
        }
    }
}
